FROM golang:alpine as builder
RUN mkdir /build
RUN apk add git
RUN go get "github.com/gorilla/mux"
RUN go get "gopkg.in/src-d/go-git.v4"
ADD . /build/
WORKDIR /build 
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o main .
FROM scratch
COPY --from=builder /build/main /app/
WORKDIR /app
CMD ["./main"]
