package main

import (
	"fmt"
	"log"
	//"os"
	"net/http"

	"github.com/gorilla/mux"
	"gopkg.in/src-d/go-git.v4"
)

func main() {
    fmt.Println("******************Starting Service***********************")
	r := mux.NewRouter()
	r.HandleFunc("/GitPull/{rest:.*}", HandleGitPull)
	//	req, _ := http.NewRequest("GET", "/foo/some/more/things", nil)
	//	r.ServeHTTP(&httptest.ResponseRecorder{}, req)
	http.Handle("/", r)
	
	// Bind to a port and pass our router in
	err := http.ListenAndServe(":80", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

}

func HandleGitPull(wri http.ResponseWriter, req *http.Request) {
	//	fmt.Println(mux.Vars(req)["rest"])
	//	fmt.Println(req.URL.Path)

	wri.WriteHeader(http.StatusOK)
	if mux.Vars(req)["rest"] == "" {
	    fmt.Println("Welcome To the GitPull Hook")
	    return
	}
	path := "/" + string(mux.Vars(req)["rest"])
	fmt.Println("Pulling at: ", path)

	// We instance\iate a new repository targeting the given path (the .git folder)
	r, err := git.PlainOpen(path)
	CheckIfError(err,wri)

	// Get the working directory for the repository
	w, err := r.Worktree()
	CheckIfError(err,wri)

	// Pull the latest changes from the origin remote and merge into the current branch
	// Info("git pull origin")
	err = w.Pull(&git.PullOptions{RemoteName: "origin"})
	CheckIfError(err,wri)

	// Print the latest commit that was just pulled
	ref, err := r.Head()
	CheckIfError(err,wri)
	commit, err := r.CommitObject(ref.Hash())
	CheckIfError(err,wri)

	fmt.Fprintf(wri, "Commit Hash %v has been pull", commit)
}

// CheckIfError should be used to naively panics if an error is not nil.
func CheckIfError(err error, w http.ResponseWriter) {
	if err == nil {
		return
	}

	//fmt.Fprintf(w, "error: %s", err)
    fmt.Println("error: ", err)

	//os.Exit(1)
}

// Info should be used to describe the example commands that are about to run.
//func Info(format string, args ...interface{}) {
//	fmt.Printf("\x1b[34;1m%s\x1b[0m\n", fmt.Sprintf(format, args...))
//}