# GitPullHook

A golang docker service to pull a git update based on path passed.

Assuming your dns and reverse proxy is all set, spin it up and use your favorite port with,
```sh
docker run --name GitPullHook -d -p 9091:80 registry.gitlab.com/bertabus/gitpullhook
```

Now when you access `cgi.myserver.com/GitPull/{restofPATHhere:.*}` the
service will go to that path and perform a git pull.